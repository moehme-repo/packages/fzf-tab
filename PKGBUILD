# Maintainer: Marty Oehme <marty.oehme@gmail.com>

pkgname=fzf-tab-git
_gitname=${pkgname%-git}
pkgver=VERSION
pkgrel=1
pkgdesc="Replace zsh's default completion selection menu with fzf."
arch=(any)
url=https://gitlab.com/moehme-repo/packages/fzf-tab.git
license=(MIT)
depends=(fzf zsh)
makedepends=(git)
provides=("$_gitname")
conflicts=("$_gitname")
source=(git+https://gitlab.com/moehme-repo/packages/fzf-tab.git#branch=master)
md5sums=('SKIP')

pkgver() {
    cd "$srcdir/$_gitname"

    _commit=$(git rev-list --count HEAD)
    _hash=$(git rev-parse --short HEAD)
    echo "r$_commit.$_hash"
}

package() {
  cd "$srcdir/$_gitname"

  typeset _plugindir="${pkgdir}/usr/share/zsh/plugins/${_gitname}"

  typeset _docdir="${pkgdir}/usr/share/doc/${_gitname}"
  typeset _licdir="${pkgdir}/usr/share/licenses/${_gitname}"

  install -dm0755 "${_plugindir}"
  install -Dm644 fzf-tab.plugin.zsh "${_plugindir}"
  install -Dm644 fzf-tab.zsh "${_plugindir}"

  install -dm0755 "${_plugindir}/lib/zsh-ls-colors"
  install -Dm644 "lib/zsh-ls-colors/ls-colors.zsh" "${_plugindir}/lib/zsh-ls-colors"
  find lib -type f -exec install -Dm644 "{}" "${_plugindir}/{}" \;

  install -dm0755 "${_plugindir}/modules"
  find modules -type f -exec install -Dm644 "{}" "${_plugindir}/{}" \;

  install -dm0755 "${_licdir}"
  install -Dm0644 LICENSE "${_licdir}"

  install -dm0755 "${_docdir}"
  install -Dm0644 README.md "${_docdir}"
}
